
let router = require("koa-router")()
let newAccountController = require("../controllers/newAccount")
let trasactionConytoller = require("../controllers/transaction")
let accountController = require("../controllers/account")

//获取创建钱包账户的页面
//私钥
//0xc9a0a622a0cbb74ba949f88b8f70f240dca280d847b37965dd5ec27f9d387730
//0xC9A0A622A0CBB74BA949F88B8F70F240DCA280D847B37965DD5EC27F9D387730
//0x3a31985253b45Ba94480cCB9Fc64cC84Db323935
//0xe260b5D7db399f3Ac22E20Abe18EbB19F08856e1

router.get("/newaccount", newAccountController.newAccountHtml)
//提交创建钱包账户的表单
router.post("/newaccount", newAccountController.newAccount)

//获取转账的页面
router.get("/transaction", trasactionConytoller.transactionHtml)
//发送交易
router.post("/sendtransaction", trasactionConytoller.sendTransaction)
//查看交易详情
router.get("/checktransaction", trasactionConytoller.checkTransactionHtml)
router.post("/checktransaction", trasactionConytoller.checkTransaction)
//上传excel
router.post("/uploadExcel",trasactionConytoller.uploadExcel)

//通过私钥解锁账户,OK 
router.post("/privateunlock", accountController.unlockAccountWithPrivate)
//通过keystore文件解锁账户,err:http500
router.post("/keystoreunlock", accountController.unlockAccountWithKeystore)



module.exports = router
