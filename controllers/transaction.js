
let { success, fail } = require("../utils/myUtils")
let web3 = require("../utils/myUtils").getweb3()
var Tx = require('ethereumjs-tx');
let XLSX = require('xlsx')
let fs = require("fs")
let sheetJsonArr;
module.exports = {

    checkTransactionHtml: async (ctx) => {
        await ctx.render("checktransaction.html")
    },

    checkTransaction: async (ctx) => {
        let hash = ctx.request.body.hash
        console.log(hash)
        let data = await web3.eth.getTransaction(hash)
        ctx.body = success(data)
    },

    transactionHtml: async (ctx) => {
        await ctx.render("transaction.html")
    },
    //解析上传的excel
    uploadExcel: async (ctx) => {
        //从request中获得文件对象
        let file = ctx.request.files.file
        //excel管理对象
        let workbook = XLSX.readFile(file.path);
        //获取所有表名字
        let sheetNames = workbook.SheetNames;
        //使用第一张表的数据,如果多张表可以用sheet1.concat(sheet2)合并
        let worksheet = workbook.Sheets[sheetNames[0]];
        let sheetJson = XLSX.utils.sheet_to_json(worksheet)
        sheetJsonArr = sheetJson;
        ctx.body = success({
            success: file.path,
            msg: "excel解析完成"
        })
    },

    sendTransaction: async (ctx) => {

        let privatekey = "0xc9a0a622a0cbb74ba949f88b8f70f240dca280d847b37965dd5ec27f9d387730"

        web3.eth.accounts.address()

        let fromaddress = "0x3a31985253b45Ba94480cCB9Fc64cC84Db323935"
        for (let index = 0; index < sheetJsonArr.length; index++) {
            const element = sheetJsonArr[index]
            let toaddress = element["钱包地址"]
            if (toaddress.length != fromaddress.length) {
                ctx.body = fail({
                    address: toaddress,
                    msg:"目标地址长度不对"
                })
                console.log(element['用户名称']+'-----'+element['钱包地址'])
                continue
            }

            //十六进制私钥
            var privateKey = new Buffer(privatekey.slice(2), 'hex')
            //获取nonce
            let nonce = await web3.eth.getTransactionCount(fromaddress)
            console.log('nonce-----------' + nonce)
            //gasPrice
            let gasPrice = await web3.eth.getGasPrice()
            //eth转wei
            let balance = web3.utils.toWei('0.0001')

            var rawTx = {
                nonce: nonce,
                gasPrice: gasPrice,
                gasLimit: '0x2710',
                to: toaddress,
                value: balance,
                data: '0x00'//转ｔｏｋｅｎ会用到的一个字段
            }

            console.log(rawTx)
            //需要将交易的数据进行预估ｇａｓ计算，然后将ｇａｓ值设置到数据参数中
            let gas = await web3.eth.estimateGas(rawTx)
            rawTx.gas = gas

            var tx = new Tx(rawTx);
            tx.sign(privateKey);

            var serializedTx = tx.serialize().toString('hex');

            // console.log(serializedTx.toString('hex'));
            // 0xf889808609184e72a00082271094000000000000000000000000000000000000000080a47f74657374320000000000000000000000000000000000000000000000000000006000571ca08a8bbf888cfa37bbf0bb965423625641fc956967b81d12e23709cead01446075a01ce999b56a8a88504be365442ea61239198e23d1fce7d00fcfc5cd3b44b7215f

            let responseData;
            await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'), function (err, data) {
                console.log(err)
                console.log(data)
                if (err) {
                    responseData = fail(err)
                }
            }).then(function (data) {
                console.log(data)
                if (data) {
                    responseData = success({
                        "blockHash": data.blockHash,
                        "transactionHash": data.transactionHash
                    })
                } else {
                    responseData = fail("交易失败")
                }
            })
        }
        console.log("发送完成")
        ctx.body = responseData
    }
}
