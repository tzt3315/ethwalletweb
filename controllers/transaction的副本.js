
let { success, fail } = require("../utils/myUtils")
let web3 = require("../utils/myUtils").getweb3()
var Tx = require('ethereumjs-tx');
let pubNonce
module.exports = {

    checkTransactionHtml: async (ctx) => {
        await ctx.render("checktransaction.html")
    },

    checkTransaction: async (ctx) => {
        let hash = ctx.request.body.hash
        console.log(hash)
        let data = await web3.eth.getTransaction(hash)
        ctx.body = success(data)
    },

    transactionHtml: async (ctx) => {
        await ctx.render("transaction.html")
    },

    sendTransaction: async (ctx) => {

        //0x13472F755C06414a4D5DCdc3A77895A3c2C9174b---account5
        //0xE45468C15F55Dadc1928329C4e5678062cec3239---center
        //0x5171000bC8d0DDB2A3A612D1E3b5877A2334dcD8---账户6
        //0xab40DD35393a0cfe60a0b9e34CaDf998cF1479F9---账户7
        //0x9dF271aec00C4E3dA270BA8F5e787c43A290028C---账户8

        let toaddresses = ["0x13472F755C06414a4D5DCdc3A77895A3c2C9174b",
            "0xE45468C15F55Dadc1928329C4e5678062cec3239",
            "0x5171000bC8d0DDB2A3A612D1E3b5877A2334dcD8",
            "0xab40DD35393a0cfe60a0b9e34CaDf998cF1479F9",
            "0x9dF271aec00C4E3dA270BA8F5e787c43A290028C"]

        //number转账数量,四个字段从前端的request中得到
        // let { fromaddress, toaddress, number, privatekey } = ctx.request.body

        let fromaddress = "0x3a31985253b45Ba94480cCB9Fc64cC84Db323935"
        let privatekey = "0xc9a0a622a0cbb74ba949f88b8f70f240dca280d847b37965dd5ec27f9d387730"
        let number = '0.0001'
        pubNonce = await web3.eth.getTransactionCount(fromaddress);

        Promise.all(
            toaddresses.map(addr => transfer(fromaddress, addr, number, privatekey, pubNonce+1))
        )
            .then((list) => {
                console.log('-------成功结束-------')
                ctx.body = JSON.stringify(list)
            })
            .catch(e => {
                console.log('-------错误结束-------')
                // ctx.body = JSON.stringify(list.map((e, i) => {
                //     return {
                //         addr: toaddresses[i],
                //         result:  '转账失败',
                //         message: e && e.message ? e.message : ''
                //     }
                // }))
                ctx.body = e && e.message ? e.message : 'fail'
            })
    }
}

function transfer(fromaddress, toaddress, number, privateKey, nonce) {
    privateKey = new Buffer(privateKey.slice(2), 'hex')
    let balance = web3.utils.toWei(number)
    let rawTx = {
        nonce: nonce,
        // gasPrice: gasPrice,
        gasLimit: '0x2710',
        to: toaddress,
        value: balance,
        data: '众筹'//转ｔｏｋｅｎ会用到的一个字段
    }
    return new Promise((resolve, reject) => {
        // sleep(2000)
            // .then(() => web3.eth.getTransactionCount(fromaddress))
            // .then(nonce => {
            //     // rawTx.nonce = nonce
            //     console.log(nonce)
                // return web3.eth.getGasPrice()
            // })
        

        // web3.eth.getTransactionCount(fromaddress)
        //     .then(nonce => {
        //         // rawTx.nonce = pubNonce
        //         console.log('[nonce]: %s', rawTx.nonce)
        //         return web3.eth.getGasPrice()
        //     })
        web3.eth.getGasPrice()
            .then(price => {
                rawTx.gasPrice = price
                // console.log('[price]: %s',price)
                console.log('rawTX: %o', rawTx)
                return web3.eth.estimateGas(rawTx)//估算gas
            })
            .then(gas => {
                rawTx.gas = gas
                let tx = new Tx(rawTx);
                tx.sign(privateKey)
                // let data = tx.serialize().toString('hex')
                return tx
            })
            .then(data => {
                return web3.eth.sendTransaction(data, function(error, result) {
                    console.log("sendSignedTransaction---err---" + err)
                    console.log("sendSignedTransaction---data---" + data)
                    if (err) {
                        reject(err)
                    }
                });

                // return web3.eth.sendSignedTransaction('0x' + data, function (err, data) {
                //     console.log("sendSignedTransaction---err---" + err)
                //     console.log("sendSignedTransaction---data---" + data)
                //     if (err) {
                //         reject(err)
                //     }
                // })
            })
            .then(data => {
                if (data) {
                    console.log('----------laile------if-----')
                    resolve(success({
                        "blockHash": data.blockHash,
                        "transactionHash": data.transactionHash
                    }))
                } else {
                    console.log('----------laile-----else------')
                    reject(new Error('fail'))
                }
            })
            .catch(reject)
    })
}

function sleep(time) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve()
        }, time);
    })
}
